﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcMicroBlog.Models;

namespace MvcMicroBlog.Controllers
{
    public class ArticleController : Controller
    {
        private readonly BlogDbDataContext db;
        public ArticleController()
        {
            db = new BlogDbDataContext();
        }


        //
        // GET: /Article/

        public ActionResult Index()
        {
            return View(db.Articles.ToList());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(); 
        }


        [HttpPost]
        public ActionResult Create(Article article)
        {
            db.Articles.InsertOnSubmit(article);
            db.SubmitChanges();
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            var article = db.Articles.Single(a => a.Id == id);
            return View(article);
        }

        [HttpPost]
        public ActionResult Edit(int id, Article article)
        {
            var articleFromDb = db.Articles.Single(a => a.Id == id);
            articleFromDb.Title = article.Title;
            articleFromDb.Content = article.Content;
            db.SubmitChanges();
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Details(int id)
        {
            var article = db.Articles.Single(a => a.Id == id);
            return View(article);
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            var article = db.Articles.Single(a => a.Id == id);
            return View(article);
        }


        [HttpPost]
        public ActionResult Delete(int id,FormCollection form)
        {
            var article = db.Articles.Single(a => a.Id == id);
            db.Articles.DeleteOnSubmit(article);
            db.SubmitChanges();
            return RedirectToAction("Index");
        }

    }
}
