﻿CREATE TABLE [dbo].[Article] (
    [Id]        INT            NOT NULL IDENTITY,
    [Title]     NVARCHAR (255) NOT NULL,
    [Content]   NVARCHAR (MAX) NOT NULL,
    [ViewCount] INT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Article] PRIMARY KEY CLUSTERED ([Id] ASC)
);

