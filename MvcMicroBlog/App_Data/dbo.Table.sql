﻿CREATE TABLE [dbo].[Article]
(
	[Id] INT NOT NULL , 
    [Title] NVARCHAR(255) NOT NULL, 
    [Content] NVARCHAR(MAX) NOT NULL, 
    [ViewCount] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_Article] PRIMARY KEY ([Id]) 
)
